ASM=nasm
ASMFLAGS=-f elf64
LD=ld

PYTHON=python3

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.o: lib.asm

dict.o: dict.asm lib.o

main.o: main.asm dict.o lib.o words.inc

program: dict.o lib.o main.o
	$(LD) -o program dict.o lib.o main.o
	
.PHONY: clean
clean:
	rm *.o
	
.PHONY: test
test:
	$(PYTHON) test.py

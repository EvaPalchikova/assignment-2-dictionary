%include "dict.inc"
%include "lib.inc"
%include "words.inc"
%include "error_codes.inc"


%define KEY_SIZE 256
%define ERROR_TEXT "key not found"
%define ERROR_LENGTH_TEXT "key is too long"


section .bss
key: resb KEY_SIZE

section .rodata
error_text: db ERROR_TEXT, 0
length_error_text: db ERROR_LENGTH_TEXT, 0

section .text
global _start
_start:
	mov rdi, key
	mov rsi, KEY_SIZE
	call read_word
		
	test rdx, rdx
	je .length_error
	
	mov rdi, rax
	mov rsi, m1
	call find_word
	
	test rax, rax
	je .error
	
	mov rdi, rax
	call get_value
	mov rdi, rax
	call print_string
	xor rdi, rdi; ok exit code
	jmp exit
	
	.length_error:
		mov rdi, length_error_text
		call print_error
		mov rdi, INCORRECT_ARGUMENT_CODE
		jmp exit
	
	.error:
		mov rdi, error_text
		call print_error
		mov rdi, KEY_NOT_FOUND_CODE
		jmp exit

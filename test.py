import unittest
from subprocess import CalledProcessError, Popen, PIPE


class DictTest(unittest.TestCase):
    def launch(self, input):
            try:
                p = Popen(['./program'], shell=None, stdin=PIPE, stdout=PIPE, stderr=PIPE)
                (output, error) = p.communicate(input.encode())
                self.assertNotEqual(p.returncode, -11, 'segmentation fault')
                return (output.decode(), error.decode(), p.returncode)
            except CalledProcessError as exc:
                self.assertNotEqual(exc.returncode, -11, 'segmentation fault')
                return (exc.output.decode(), error.decode(), exc.returncode)
                
                
    def test_program(self):
        tests = {
            'key' : ('value', '', 0),
            'key2' : ('value2', '', 0),
            'wrong_key': ('', 'key not found', 1),
            'looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong_key': ('', 'key is too long', 2)
        }
        for test_key, (test_output, test_error, test_code) in tests.items():
            (output, error, code) = self.launch(test_key)
            self.assertEqual(output, test_output, 'wrong ounput \n input=%s \n output=%s \n correct output=%s' % (repr(test_key), repr(output), repr(test_output)))
            self.assertEqual(error, test_error, 'wrong error \n input=%s \n error=%s \n correct error=%s' % (repr(test_key), repr(error), repr(test_error)))
            self.assertEqual(code, test_code, 'wrong error code \n input=%s \n code=%s \n correct code=%s' % (repr(test_key), repr(code), repr(test_code)))

            

unittest.main()

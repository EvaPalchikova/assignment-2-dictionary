%include "lib.inc"
%define SIZE 8

global find_word
global get_value

section .text
; аргументы: rdi -- указатель на нуль-терминированную строку (ключ), rsi -- указатель на начало словаря
; возвращает: rax -- адрес начала вхождения в словарь (не значения), 0 -- если ключ не найден
find_word:
	push r12
	.loop:
		mov r12, rsi
		add rsi, SIZE
		
		push rdi
		call string_equals
		pop rdi
		
		test rax, rax
		jne .found
		
		mov rsi, [r12]
		test rsi, rsi
		je .not_found
		
		jmp .loop
		
	.found:
		mov rax, r12
		jmp .end
		
	.not_found:
		xor rax, rax

	.end:
		pop r12
		ret
		
; аргументы: rdi -- указатель на элемент словаря
; возвращает: rax -- указатель на значение 
get_value:
	add rdi, SIZE
	
	call string_length
	add rdi, rax
	inc rdi
	
	mov rax, rdi
	ret
